/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found.  It
 * should not be modified by hand.
 */

package com.example.restaurantfinder;

public final class R {
    public static final class array {
        public static final int cuisines=0x7f040000;
    }
    public static final class attr {
    }
    public static final class drawable {
        public static final int ic_launcher=0x7f020000;
    }
    public static final class id {
        public static final int cuisine=0x7f070004;
        public static final int cuisine_label=0x7f070003;
        public static final int get_reviews_button=0x7f070005;
        public static final int intro_blurb_criteria=0x7f070000;
        public static final int location=0x7f070002;
        public static final int location_label=0x7f070001;
        public static final int spinner_item=0x7f070006;
    }
    public static final class layout {
        public static final int review_criteria=0x7f030000;
        public static final int spinner_view=0x7f030001;
        public static final int spinner_view_dropdown=0x7f030002;
    }
    public static final class string {
        public static final int alert_label=0x7f050016;
        public static final int app_name_criteria=0x7f050000;
        public static final int app_name_review=0x7f050002;
        public static final int app_name_reviews=0x7f050001;
        public static final int app_short_name=0x7f050003;
        public static final int author_label=0x7f050013;
        public static final int cuisine_label=0x7f05000e;
        public static final int get_reviews_button_label=0x7f05001b;
        public static final int intro_blurb_criteria=0x7f05000a;
        public static final int intro_blurb_detail=0x7f05000b;
        public static final int link_label=0x7f050014;
        public static final int location_entry_label=0x7f05000c;
        public static final int location_label=0x7f05000d;
        public static final int location_not_supplied_message=0x7f050017;
        public static final int menu_call_review=0x7f050007;
        public static final int menu_change_criteria=0x7f050008;
        public static final int menu_get_next_page=0x7f050009;
        public static final int menu_get_reviews=0x7f050004;
        public static final int menu_map_review=0x7f050006;
        public static final int menu_web_review=0x7f050005;
        public static final int name_label=0x7f050011;
        public static final int no_link_message=0x7f05001a;
        public static final int no_location_message=0x7f050019;
        public static final int no_phone_message=0x7f050018;
        public static final int phone_label=0x7f050015;
        public static final int rating_label=0x7f050010;
        public static final int review_label=0x7f05000f;
        public static final int title_label=0x7f050012;
    }
    public static final class style {
        /** 
        Base application theme for API 11+. This theme completely replaces
        AppBaseTheme from res/values/styles.xml on API 11+ devices.
    
 API 11 theme customizations can go here. 
         */
        public static final int AppBaseTheme=0x7f060007;
        public static final int edit_text=0x7f060002;
        public static final int intro_blurb=0x7f060000;
        public static final int label=0x7f060001;
        /**   this is the style for the container ListView, not the rows 
  the rows are defined in code, see ReviewAdapter.ReviewListView 
         */
        public static final int review_list=0x7f060006;
        public static final int spinner_item=0x7f060004;
        public static final int spinner_item_dropdown=0x7f060005;
        public static final int view_text=0x7f060003;
    }
}
