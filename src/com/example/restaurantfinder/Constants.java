package com.example.restaurantfinder;

public class Constants {

	public static final String INTENT_ACTION_VIEW_DETAIL = "com.example.restaurantfinder.VIEW_DETAIL";
    public static final String INTENT_ACTION_VIEW_LIST = "com.example.restaurantfinder.VIEW_LIST";

    public static final String LOGTAG = "RestaurantFinder";
    public static final String PREFS = "RCP";

    public static final String STARTFROM_EXTRA = "com.example.restaurantfinder.StartFrom";
}